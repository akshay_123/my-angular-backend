var dbObject=require('../Resources/dbproperties');//reference of dbConnection.js file

var Task={
    getAllTask:function(callback){
        return dbObject.query('select * from task',callback);

    },
    //Add task method
    addTask:function(task,callback){
        return dbObject.query('insert into task values(?,?,?)',[task.id,task.title,task.status],callback);
    },

    //update task
    updateTask:function(id,task,callback){
      //  console.log(id);
        return dbObject.query('update task set title=?,status=? where id=?',[task.title,task.status,id],callback);
    },

    delteTask:function(id,callback){
        return dbObject.query('Delete from task where id=?',[id],callback);

    },

    getTaskById:function(id,callback){
        return dbObject.query('select * from task where id=?',[id],callback);

    }
};

module.exports=Task;
