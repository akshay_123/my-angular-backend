var dbObject=require('../Resources/dbproperties');

var user={
    getAllUsers:function(callback){
        var sql='select * from users;';
        return dbObject.query(sql,callback);
    },
    addUser:function(user,callback){
        var sql='insert into users values(?,?,?,?);';
        return dbObject.query(sql,[user.user_email,user.user_name,user.user_password,user.user_mbl_no],callback);
    },
    updateUserByUserEmail:function(userEmail,user,callback){
        var sql='update users set user_name=?,user_password=?,user_mbl_no=? where user_email=?;';
        return dbObject.query(sql,[user.user_name,user.user_password,user.user_mbl_no,userEmail],callback);
    },
    deleteUserByUserEmail:function(userEmail,callback){
        var sql='delete from users where user_email=?;';
        return dbObject.query(sql,[userEmail],callback);
    }
}

module.exports=user;