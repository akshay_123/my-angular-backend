var dbObject=require('../Resources/dbproperties');

var product={
    //Get all products
    getAllProduct:function(callback){
        return dbObject.query('select * from product',callback);
    },

    //Insert product
    addProduct:function(product,callback){
        var query='insert into product values(?,?,?,?,?,?,?);'
        return dbObject.query(query,[product.pro_id,product.pro_name,product.pro_price,product.pro_desc,product.pro_qty,product.pro_mfg,product.pro_img],callback);
    },

    //update product by productId
    updateProductByProductId:function(productId,product,callback){
        console.log(productId);
        var sql='update product set pro_name=?,pro_price=?,pro_desc=?,pro_qty=?,pro_mfg=?,pro_img=? where pro_id=?;';
        return dbObject.query(sql,[product.pro_name,product.pro_price,product.pro_desc,product.pro_qty,product.pro_mfg,product.pro_img,productId],callback);

    },

    //Delete product by productId
    deleteProductByProductId:function(productId,callback){
        var sql='Delete from product where pro_id=?;';
        return dbObject.query(sql,[productId],callback);
    }
}

module.exports=product;