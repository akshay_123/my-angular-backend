var express=require('express');
const { route } = require('.');
//we require router to navigate a request to correct model method based on reuest method and endpoint. so
//that method get called and query is executed
var router=express.Router();
var taskModel=require('../models/Task');

//router to get all task
router.get('/',function(req,res,next){
    taskModel.getAllTask(function(err,rows){
        if (err) {
            res.json(err);
            
        } else{
            res.json(rows);
        }
    })

});

//router to add task
router.post('/',function(req,res,next){
    taskModel.addTask(req.body,function(err,rows){
        if (err) {
            res.json(err);
            
        } else {
            res.json(rows);
            
        }
    })
});

//router to update task
router.put('/:id',function(req,res,next){
    taskModel.updateTask(req.params.id,req.body,function(err,rows){
        if (err) {
            res.json(err);
            
        } else {
            res.json(rows);
            
        }
    })
});

//router to delete task
router.delete('/:id',function(req,res,next){
    taskModel.delteTask(req.params.id,function(err,rows){
        if (err) {
            res.json(err);
            
        } else {
            res.json(rows);
            
        }
    })
});

//Get task By Id 
router.get('/:id',function(req,res,next){
    taskModel.getTaskById(req.params.id,function(err,rows){
        if (err) {
            res.json(err);
            
        } else {
            res.json(rows);
            
        }
    })
});

module.exports=router;