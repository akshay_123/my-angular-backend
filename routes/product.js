var express=require('express');
var router = express.Router();
var productModel=require('../models/Product');

//Get all product router
router.get('/',function(req,res,next){
    productModel.getAllProduct(function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

//Add product router
router.post('/',function(req,res,next){
    productModel.addProduct(req.body,function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

//update router
router.put('/:productId',function(req,res,next){
    productModel.updateProductByProductId(req.params.productId,req.body,function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

//Delete Router
router.delete('/:productId',function(req,res,next){
    productModel.deleteProductByProductId(req.params.productId,function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

module.exports=router;