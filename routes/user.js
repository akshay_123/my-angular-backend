var expres=require('express');
var router=expres.Router();
var userModel=require('../models/User');

//Get all user router
router.get('/',function(req,res,next){
    userModel.getAllUsers(function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

//Add user router
router.post('/',function(req,res,next){
    userModel.addUser(req.body,function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

//update user router
router.put('/:userEmail',function(req,res,next){
    userModel.updateUserByUserEmail(req.params.userEmail,req.body,function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

//Delete user Router
router.delete('/:userEmail',function(req,res,next){
    userModel.deleteUserByUserEmail(req.params.userEmail,function(err,rows){
        if (err) {
            res.json(err);
        } else {
            res.json(rows);
            
        }

    })
});

module.exports=router;
